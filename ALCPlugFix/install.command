#!/bin/bash


path=${0%/*}
sudo cp -a "$path/ALCPlugFix" /usr/local/bin
sudo chmod 755 /usr/local/bin/ALCPlugFix
sudo chown root:wheel /usr/local/bin/ALCPlugFix
sudo cp -a "$path/hda-verb" /usr/local/bin
sudo chmod 755 /usr/local/bin/hda-verb
sudo chown root:wheel /usr/local/bin/hda-verb
sudo cp -a "$path/good.win.ALCPlugFix.plist" /Library/LaunchDaemons/
sudo chmod 644 /Library/LaunchDaemons/good.win.ALCPlugFix.plist
sudo chown root:wheel /Library/LaunchDaemons/good.win.ALCPlugFix.plist
sudo launchctl load /Library/LaunchDaemons/good.win.ALCPlugFix.plist
echo 'Installing the ALCPlugFix daemon is complete!'
echo 'Please wait while rebuilding cache...'
sudo rm -rf /Library/Caches/com.apple.kext.caches/Startup/kernelcache
sudo rm -rf /Library/PrelinkedKernels/prelinkedkernel
sudo touch /Library/Extensions/ && sudo kextcache -u /
echo 'The installation process is over, please restart your computer! ! ! '
bash read -p '按任何键退出'