# ALCPlugFix Fix

## What is this?

A lot of laptops running AppleALC with combo jacks have this weird issue where sound will come out muffled/distroted when headphones are plugged in unless you switch to the "Line In" Mic input in System Preferences.

I've had this issue for bloody ages.

There is a more up-to-date version of this fix that kinda exists, but it falls into the modern hackintosh trap of complicated config.plists with barely any guidance.

IF you know what you're doing (somehow), you can find it here: https://github.com/black-dragon74/ALCPlugFix-Swift

This is an edit of the original ALCPlugFix to respect correct directories in macOS Catalina and above. Also I translated it to English.

## How to use 

Run install.command

That's it, that's all she wrote